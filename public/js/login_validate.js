//$.validator.setDefaults({
    //submitHandler: function() { }
//});

$.validator.addMethod('username', function(value, element, params) {
    if( $("#first_name").val() == "" || value == "") {
        $("#first_name").focus();
	    return false;
	} else {
      return true;
	}
}, 'Please provide first and last name');

$.validator.addMethod('datebirth', function(value, element, params) {
    if( $("#birth_date").val() == "" && value == "") {
	    return false;
	} else if($("#birth_date").val() != "" && value != "") {
        return false;
	} else {
        return true;
	}

}, 'Please select any one date');

$().ready(function() {
    // validate the comment form when it is submitted
    //$("#login").validate();
    // validate signup form on keyup and submit
    $("#register").validate({
        rules: {
            est_date: "datebirth",
			last_name: "username",
            password: {
                required: true,
                minlength: 5
            },
            password_verify: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
        },

        messages: {           
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            password_verify: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address"
        }
    });
});
