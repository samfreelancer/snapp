<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /*protected function _initResourceAutoloader()
    {
         $autoloader = new Zend_Loader_Autoloader_Resource(array(
            'basePath'  => APPLICATION_PATH,
            'namespace' => 'Application',
         ));

         $autoloader->addResourceType( 'model', 'models', 'Model');
         return $autoloader;
    }*/

    protected function _initDatabase(){
        // get config from config/application.ini
        $config = $this->getOptions();

        $db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);

        //set default adapter
        Zend_Db_Table::setDefaultAdapter($db);

        //save Db in registry for later use
        Zend_Registry::set("db", $db);
    }

    protected function _initAutoLoad() {
        Zend_Loader_Autoloader::getInstance()->registerNamespace('Default_');
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'basePath'  => APPLICATION_PATH,
            'namespace' => '',
        ));
        $autoloader->addResourceType('form', 'modules/default/forms/', 'Form');
        $autoloader->addResourceType('plugin', 'modules/default/plugins/', 'Plugin');
        $autoloader->addResourceType('model', 'modules/default/models/', 'Model');

        return $autoloader;
    }

    protected function _initLogger() {
       $this->bootstrap("log");
       $logger = $this->getResource("log");
       Zend_Registry::set("logger", $logger);
    }
}