<?php
class Default_IndexController extends Zend_Controller_Action {

    public function init(){

    }

    public function indexAction() {
        $this->view->headTitle('Login');
        if (Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('role/list');
        $this->view->headLink()->prependStylesheet($this->view->baseUrl() . '/public/css/jquery-ui.css');
        $this->view->headScript()->appendFile($this->view->baseUrl() . '/public/js/jquery-ui.js');
        $form    = new Form_Login();

        // Check if we have a POST request
        if ($this->_request->isPost()) {
            // Get our form and validate it
            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {
                $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
                $authAdapter->setTableName('users')
                    		->setIdentityColumn('email')
                    		->setCredentialColumn('password');

                // pass to the adapter the submitted username and password
                $authAdapter->setIdentity($formData['email'])
                			->setCredential(md5($formData['password']));

                $auth   = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);

                if($result->isValid()){ // is the user a valid one?
                    $userInfo    = $authAdapter->getResultRowObject(null, 'password');

                    // the default storage is a session with namespace Zend_Auth
                    $authStorage = $auth->getStorage();
                    $authStorage->write($userInfo);

                    $this->_redirect('role/list');
                } else {
                    $this->view->error = array('login' => '0');
                    $form->populate($formData);
                    Zend_Auth::getInstance()->clearIdentity();
                }
            } else {
                $this->view->error = $form->getMessages();
                $form->populate($formData);
            }
        }

        // Retreving messages
        $message = $this->_helper->flashMessenger->getMessages();
        $this->view->message = implode('<br>',$message);
        $this->_helper->flashMessenger->clearCurrentMessages();
        $this->view->form = $form;
    }

    

    public function registerAction() {
        $form = new Form_Register();

        // Check if we have a POST request
        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if ($form->isValid($formData)) {
                $user      = new Model_User();
                $checkData = $user->checkEmail($formData['email']);
                if(count($checkData)>0) { 
                    $this->view->error = array('email' => 'Email address alredy exist');
                    $this->_helper->flashMessenger->clearCurrentMessages();
                    $this->view->reg_form = $form;
                    $this->render('index');
                    exit;
                }
                
                $formData['birth_date'] = date('Y-m-d', strtotime($formData['birth_date']));
                
                $insertId = $user->addUser($formData);
                if($insertId) {
					
					// StartInsert user id info default to on launch
					$userPrivacy = new Model_UserSecurity();
					$userDetail = array(
							'user_id' => $insertId,
							'ask_launch' => 1
					);
					
					$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
                    $authAdapter->setTableName('users')
                        		->setIdentityColumn('email')
                        		->setCredentialColumn('password');
                    
                    // pass to the adapter the submitted username and password
                    $authAdapter->setIdentity($formData['email'])
                        		->setCredential(md5($formData['password']));

                    $auth        = Zend_Auth::getInstance();
                    $result      = $auth->authenticate($authAdapter);
                    $userInfo    = $authAdapter->getResultRowObject(null, 'password');
                    
                    // the default storage is a session with namespace Zend_Auth
                    $authStorage = $auth->getStorage();
                    $authStorage->write($userInfo);

                    $this->_redirect('role/list');
                }
            } else {
                $this->view->error = $form->getMessages();
                $form->populate($formData);
            }
        }

        // Retreving messages
        $message = $this->_helper->flashMessenger->getMessages();
        $this->view->message = implode('<br>',$message);
        $this->_helper->flashMessenger->clearCurrentMessages();
        $this->view->reg_form = $form;

        $this->render('index');
    }
}