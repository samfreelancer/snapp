<?php
class Default_RoleController extends Zend_Controller_Action {
	
	private $userId   = '';
	private $email = '';
	
	/*
	 * @todo: These settings should manage under config file
	 */
	private $paging = true;
	private $searching = true;
	private $ordering = true;
	private $pageLength = 2;
	
	// Call befor any action and check is user login or not
	public function preDispatch() {
		date_default_timezone_set('America/Los_Angeles');
		if (!Zend_Auth::getInstance()->hasIdentity())
			$this->_redirect('/index/index');
	
		$this->userId = Zend_Auth::getInstance()->getStorage()->read()->user_id;
		$this->email = Zend_Auth::getInstance()->getStorage()->read()->email;
		
	}
	
    public function init(){

    }

    /*
     * list all the items
     */
    public function listAction(){
    	$this->view->headTitle('List Roles');
    	//$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/css/bootstrap.min.css');
    	$this->view->headLink()->prependStylesheet($this->view->baseUrl() . '/public/css/dataTables.bootstrap.css');
    	$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/js/jquery.dataTables.js');
    	$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/js/dataTables.bootstrap.js');
    	try {
	    	$roleModel = New Model_Role();
	    	
	    	$userRole = $roleModel->listRole($this->userId);
	    	$roles = array();
	    	$count = 0;
	    	foreach ($userRole as $role){
	    		$roles[$count]['id'] = $role['id'];
	    		$roles[$count]['name'] = $role['name'];
	    		$roles[$count]['created'] = $role['created'];
	    		$roles[$count]['checkbox'] = '<div class="text-center"><input type="checkbox" name="Delete[]" id="delete_role_'.$role['id'].'" value="'.$role['id'].'"></div>';
	    		$roles[$count]['update'] = '<a href="#" class="cls-update"><i class="glyphicon glyphicon-check"></i> Update</a>';
	    		$count++;
	    	}
	    	$this->view->roles = Zend_Json::encode($roles);
	    	$this->view->paging = $this->paging;
	    	$this->view->searching = $this->searching;
	    	$this->view->ordering = $this->ordering;
	    	$this->view->pageLength = $this->pageLength;
    	} catch ( Exception $e ) {
            $logger = Zend_Registry::get('logger');
            $logger->log($e->getMessage(), Zend_Log::ERR);
        }
    	
    	
    }
    
    /*
     * Add an item
    */
    public function addAction(){
    	if ($this->_request->isPost()) {
    		// Get our form and validate it
    		$formData = $this->_request->getPost();
    		$roleModel = New Model_Role();
    		$ifExist = $roleModel->checkRoleExist($formData['rname']);
    		
    		if(empty($ifExist)){ // role doesn't exist
    			
    			if($roleModel->addNewRole($formData['rname'], $this->userId)){
    				$this->_redirect('role/list');
    			}else{
    				$this->view->error = array('random' => 1);
    			}
    		}else{
    			$this->view->error = array('duplicate' => 1);
    		}
    	}
    }
    
    /*
     * update an item
     * @todo: update feature still need to implement
    */
    public function updateAction(){
    	 
    }
    
    /*
     * Delete an item
    */
    public function deleteAction(){
    	if ($this->getRequest()->isXmlHttpRequest()) {
    		$this->_helper->layout()->disableLayout();
    		$this->_helper->viewRenderer->setNoRender(true);
    		
    		$delete = $this->_request->getPost();
    		
    		if (count($delete['Delete'])){
    			$ids = implode(',', $delete['Delete']);
    			$roleModel = New Model_Role();
    			$result = $roleModel->removeRole($this->userId, $ids);
    			if ($result){
    				echo Zend_Json::encode(array('status'=>true,'message' => 'Success'));
    			} else {
    				echo Zend_Json::encode(array('status'=>false,'message' => 'Some problem in delete')); 
    			}
    		} else {
    			echo Zend_Json::encode(array('status'=>false, 'message' => 'Please select at least a role to delete'));
    		}
    	}
    }
}