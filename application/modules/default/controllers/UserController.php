<?php
class Default_UserController extends Zend_Controller_Action {
    private $userId   = '';
    private $email = '';

    // Call befor any action and check is user login or not
    public function preDispatch() {
        date_default_timezone_set('America/Los_Angeles');
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/index/index');

        $this->userId = Zend_Auth::getInstance()->getStorage()->read()->user_id;
        $this->email = Zend_Auth::getInstance()->getStorage()->read()->email;
    }

    public function indexAction() {

    }

    /*
     * Change user password
     */
    public function chpwdAction() {
    	$this->view->headTitle('Change Password');
    	$form    = new Form_Chpwd();
    	
    	// Check if we have a POST request
    	if ($this->_request->isPost()) {
    		// Get our form and validate it
    		$formData = $this->_request->getPost();
    		
    		if ($form->isValid($formData)) {
    			if ($formData['newpwd'] === $formData['cnfrmpwd']){
    				
    				$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
    				$authAdapter->setTableName('users')
			    				->setIdentityColumn('email')
			    				->setCredentialColumn('password');
    				
    				// pass to the adapter the submitted username and password
    				$authAdapter->setIdentity($this->email)
    							->setCredential(md5($formData['password']));
    				
    				$auth   = Zend_Auth::getInstance();
    				$result = $auth->authenticate($authAdapter);
    				
    				if ($result->isValid()){ // is the user a valid one?
    					$user	= new Model_User();
    					$userDetail = array(
    							'password'	=> md5($formData['newpwd']),
    					);
    					
    					$status = $user->updateProfile($userDetail, $this->userId);
    					$this->view->error = array('success' => '0');
    					$this->_helper->flashMessenger->addMessage('Successfully changed password');
    					//$this->_redirect('user/chpwd');
    				} else {
    					$this->view->error = array('login' => '0');
    					//$form->populate($formData);
    					Zend_Auth::getInstance()->clearIdentity();
    				}
    			} else {
    				$this->view->error = array('pwd_mismatch' => '0');
    				//$form->populate($formData);
    			}
    		} else {
    			$this->view->error = $form->getMessages();
    			//$form->populate($formData);
    		}
    	}
    
    	// Retreving messages
    	$message = $this->_helper->flashMessenger->getMessages();
    	$this->view->message = implode('<br>',$message);
    	$this->_helper->flashMessenger->clearCurrentMessages();
    	$this->view->form = $form;
    }
    
    /*
     * Allow user to delete their own account
     */
    public function delaccAction(){
    	$this->view->headTitle('Delete Account');
    	if ($this->_request->isPost()) {
    		$user	= new Model_User();
    		if ($user->remove($this->userId, $this->email)){
    			Zend_Auth::getInstance()->clearIdentity();
    			$this->_redirect('/index/index');
    		}
    	}
    }
    
    /*
     * Logout user from system
     */
    public function logoutAction() {
    	Zend_Auth::getInstance()->clearIdentity();
    	Zend_Session::destroy();
    	// back to login page
    	$this->_redirect('/index');
    }
    
}