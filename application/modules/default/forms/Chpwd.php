<?php
class Form_Chpwd extends Zend_Form {
    // Creating forms
    public function init() {
    	$this->setAction($this->getView()->baseUrl() . '/index/chpwd');

    	$this->addElement('password', 'password', array(
    			'label'      => 'Current Password',
    			'required'   => true,
    			'filters'    => array('StringTrim'),
    			'validators' => array(
    					'NotEmpty'
    			)
    	));
    	$this->addElement('Password', 'newpwd', array(
    			'label'      => 'New Password',
    			'required'   => true,
    			'filters'    => array('StringTrim'),
    			'validators' => array(
    					'NotEmpty'
    			)
    	));
        $this->addElement('password', 'cnfrmpwd', array(
            'label'      => 'Confirm Password',        	 
 			'required'   => true,
            'filters'    => array('StringTrim'),
          	'validators' => array(
						'NotEmpty'
						)        
        ));

        $this->addElement('submit', 'login', array (
            'label'       => 'Login',        	
        ));
    }
}
?>