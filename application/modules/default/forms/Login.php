<?php
class Form_Login extends Zend_Form {
    // Creating forms
    public function init() {
    	$this->setAction($this->getView()->baseUrl() . '/index/login');

    	$this->addElement('text', 'email', array(            
            'label'      => 'Email',
 			'required'   => true,
            'filters'    => array('StringTrim'),
          	'validators' => array(
						'NotEmpty', 'EmailAddress'
						)        
        ));

        $this->addElement('password', 'password', array(
            'label'      => 'Password',        	 
 			'required'   => true,
            'filters'    => array('StringTrim'),
          	'validators' => array(
						'NotEmpty'
						)        
        ));

        $this->addElement('submit', 'login', array (
            'label'       => 'Login',        	
        ));
    }
}
?>