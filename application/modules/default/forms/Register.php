<?php
class Form_Register extends Zend_Form {
    // Creating forms
    public function init() {
        $this->setAction($this->getView()->baseUrl() . '/index/register');

        $this->addElement('text', 'first_name', array(
            'label'      => 'First Name',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                                'NotEmpty'
                            )
        ));

        $this->addElement('text', 'last_name', array(
            'label'      => 'last_name',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                                'NotEmpty'
                            )
        ));

        $this->addElement('text', 'email', array(
            'label'      => 'email',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                                'NotEmpty', 'EmailAddress'
                            )
        ));

        $this->addElement('password', 'password', array(
            'label'      => 'Password',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                                'NotEmpty',
                            )
        ));

        $this->addElement('password', 'password_verify', array(
            'label'      => 'Verify Password',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array('NotEmpty',
                                array('identical', true, array('password'))
                            )
        ));

        $this->addElement('submit', 'register', array (
            'label'      => 'Sign Up',
        ));
    }
}
?>