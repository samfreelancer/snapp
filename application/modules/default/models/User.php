<?php
class Model_User extends Zend_Db_Table_Abstract {
    protected $_name      = 'users';
    public $_errorMessage = '';

    public function getUserDetails($userId) {
        $select = $this->select()->from(array('u' => $this->_name))
                     ->where('user_id = ?', $userId);

        $result = $this->fetchRow($select);

        return ($result && sizeof($result)>0)? $result: false ;
    }

    public function checkEmail($email) {
        $sql = $this->select()->from($this->_name, array('user_id', 'email'))
                   ->where('email = ?', $email);

       return $row = $this->fetchRow($sql);
    }
	
    public function remove($user_id, $email) {
    	$where = array();
    	$where[] = $this->getAdapter()->quoteInto('user_id = ?', $user_id);
    	$where[] = $this->getAdapter()->quoteInto('email = ?', $email);
    	return $this->delete($where);
    }
    
    public function addUser($user) {
        $data = array (
                   'first_name' => $user['first_name'],
                   'last_name'  => $user['last_name'],
                   'email'      => $user['email'],
                   'password'   => md5($user['password']),
                   'birth_date' => $user['birth_date'],
                   'sex'        => $user['sex']
        );

        $insertedData = $this->insert($data);

        return (intval($insertedData)>0) ? $insertedData  : false ;
    }

    public function updateProfile($userData, $userId) {
        return $this->update($userData, $this->_db->quoteInto('user_id = ?', $userId));
    }
}
?>