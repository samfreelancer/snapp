<?php
class Model_Role extends Zend_Db_Table_Abstract {
	protected $_name      = 'roles';
	public $_errorMessage = '';
	
	
	public function addNewRole($name, $user) {
		$data = array (
				'name' => $name,
				'user_id'  => $user,
				'created'  => date('Y-m-d H:i:s')
		);
		$insertedData = $this->insert($data);
		return (intval($insertedData)>0) ? $insertedData  : false ;
	}
	
	public function checkRoleExist($name){
		$sql = $this->select()
					->from($this->_name, array('id'))
					->where('name = ?', $name);
		
		return $this->fetchRow($sql);
	}
	
	public function removeRole($user_id, $role_id) {
    	$where = array();
    	$where[] = $this->getAdapter()->quoteInto('user_id IN (?)', $user_id);
    	$where[] = $this->getAdapter()->quoteInto('id = ?', $role_id);
    	return $this->delete($where);
    }
    
    public function listRole($userId){
    	
    	$roles = $this->select()
    					->from(array('r' => $this->_name),array('r.id', 'r.name', 'r.created'))
    					->where('r.user_id = ?', $userId)
    					->order('r.created DESC');
    	return $this->fetchAll($roles);
    
    }
}